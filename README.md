# Tempus Web Demonstration

Source code and deployment scripts for the Web demo of
[Tempus](https://github.com/Ifsttar/Tempus/), a C++ framework to develop
multimodal path planning requests.


## Installation

Installation is done through ansible scripts. See `ansible/` directory.

A Vagrantfile is provided for testing purpose. Just execute `vagrant up`.
