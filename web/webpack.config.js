let path = require('path');
let webpack = require('webpack');

module.exports = {
  entry: {
    'tempus_osm': [path.resolve(__dirname, 'js/index_osm.js') ],
  },
  devtool: 'source-map',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
    library: '[name]',
    libraryTarget: 'umd',
    umdNamedDefine: true
  },
  devServer: {
    publicPath: '/dist/'
  },
  plugins: [
          new webpack.ProvidePlugin({
              $: "jquery",
              jQuery: "jquery"
          })
      ]

};

