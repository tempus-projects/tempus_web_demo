// OSM map
import ol from 'openlayers';
import moment from 'moment';
import momentDurationFormat from 'moment-duration-format';
import datepicker from 'air-datepicker';
import datepickerI18n from 'air-datepicker/dist/js/i18n/datepicker.en.js';
import settings from './settings';
import PointPicker from './PointPicker';

// FIXME so we can't have more than 9 transports modes ?
let TRANSPORT_MODE_COLOR = ['#e54444', '#95e544', '#44e5e5', '#9544e5', '#F5BD39', '#006637', '#0371FF', '#ff8efe', '#ecea48'];

// lyon extent in EPSG:4326
// xMin,yMin 4.58959,45.5755 : xMax,yMax 5.10403,45.9178
let [lyonLeft, lyonBottom, lyonRight, lyonTop] = ol.proj.transformExtent([ 4.58959, 45.5755, 5.10403, 45.9178], 'EPSG:4326', 'EPSG:3857');
console.log(lyonLeft, lyonRight, lyonBottom, lyonTop);
let openLayer = new ol.layer.Tile({
  source: new ol.source.OSM(),
});

let center = [(lyonRight - lyonLeft) / 2 + lyonLeft, (lyonTop - lyonBottom) / 2 + lyonBottom];
let resolution = (lyonRight - lyonLeft) / (256 * 2)
let view = new ol.View({
  center: center,
  resolution: resolution,
  maxResolution: resolution,
  extent: [lyonLeft, lyonBottom, lyonRight, lyonTop],
});

let map = new ol.Map({
  layers: [openLayer],
  target: 'map_container',
  view
});

// Controls
let itineraryPointSource = new ol.source.Vector({});
map.addLayer(new ol.layer.Vector({ source: itineraryPointSource }));


let pickingPoints = {
  'start': {
    label: new ol.style.Style({
      text: new ol.style.Text({
        text: '', // \uF1E5
        font: '40px "Ionicons"',
        offsetY: -17,
        fill: new ol.style.Fill({ color: '#4EB5E6' }),
      }),
    }),
  },
  'dest': {
    label: new ol.style.Style({
      text: new ol.style.Text({
        text: '', // \uF1E5
        font: '40px "Ionicons"',
        offsetY: -17,
        fill: new ol.style.Fill({ color: '#95E063' }),
      }),
    }),
  },
  'parking_location': {
    label: new ol.style.Style({
      text: new ol.style.Text({
        text: '', // \uF1E5
        font: '40px "Ionicons"',
        offsetY: -17,
        fill: new ol.style.Fill({ color: '#F2A933' }),
      }),
    }),
  },
};

for (let id in pickingPoints) {
  let pd = pickingPoints[id];
  let pp = new PointPicker(map, itineraryPointSource, pd.label);
  document.getElementById(`place_${id}`).addEventListener('click', () => {
    pp.startPicking();
  });

  let inputElem = document.getElementById(id);
  if (inputElem.value) {
    pp.place(inputElem.value.split(';').map(parseFloat), 'EPSG:4326');
  }
  pp.addEventListener('placed', (evt) => {
    console.log(evt);
    inputElem.value = pp.getCoordinates('EPSG:4326').join(';');
  });

  inputElem.addEventListener('input', (evt) => {
    pp.place(inputElem.value.split(';').map(parseFloat), 'EPSG:4326');
  });
  pd.pp = pp;
}

let getItinerary = document.getElementById('get_itinerary');
getItinerary.disable = true;

function enableGetItinerary() {
  if (pickingPoints['start'].pp.getCoordinates('EPSG:4326') && pickingPoints['dest'].pp.getCoordinates('EPSG:4326')) {
    getItinerary.disabled = false;
  }
}

pickingPoints['start'].pp.addEventListener('placed', enableGetItinerary);
pickingPoints['dest'].pp.addEventListener('placed', enableGetItinerary);

let itineraryFeatures = new ol.Collection();
map.addLayer(new ol.layer.Vector({
  source: new ol.source.Vector({ features: itineraryFeatures }),
  style: (feature) => {
    console.log(feature);
    return new ol.style.Style({
      stroke: new ol.style.Stroke({
        color: TRANSPORT_MODE_COLOR[feature.getProperties()['transport_mode'].id],
        width: 2
      })
    });
  },
}));


let updateItinerary = (function() {
  let itineraryContainer = document.getElementById('itinerary');
  let metadataContainer = itineraryContainer.querySelector('.itinerary_metadata');
  let stepList = itineraryContainer.querySelector('.step_list');

  itineraryContainer.querySelector('.close').addEventListener('click', (e) => {
    e.preventDefault();
    itineraryContainer.classList.remove('visible');
    return false;
  });

  return function(jsonItinerary) {
    while (stepList.firstChild) {
      stepList.removeChild(stepList.firstChild);
    }

    stepList.innerHTML = `
      <li>
        <span>Mode</span>
        <span>Cost</span>
        <span>End movement</span>
        <span>Street name</span>
      </li>
      `;

    // Start time
    // TODO check parsing once I have real date
    let startDateTime = moment(jsonItinerary.properties['starting_date_time']);
    if (!startDateTime.isValid()) {
      startDateTime = moment(new Date());
    }

    // aggregated time for duration and end time
    let aggregatedTimeMin = 0;

    let startMovement;
    for (let f of jsonItinerary.features) {
      let step = document.createElement('li');
      let props = f.properties;

      // transport mode
      let modeElem = document.createElement('span');
      let transportMode = props['transport_mode'].name;
      //modeElem.classList.add(`mode_${transportMode}`);
      // TODO nicer text
      modeElem.innerHTML = transportMode;
      step.appendChild(modeElem);

      // costs
      // aggregate for end time
      let costDuration = props['costs']['CostDuration'];
      aggregatedTimeMin += costDuration;
      let costElem = document.createElement('span');
      costElem.classList.add('cost');
      // TODO nicer texts
      costElem.innerHTML = costDuration;
      step.appendChild(costElem);

      // start movement, if applicable
      let moveElem = document.createElement('span');
      if (startMovement) {
        moveElem.classList.add(`move_${startMovement}`);
        // TODO nicer text
        moveElem.innerHTML = startMovement;
      }
      step.appendChild(moveElem);

      // road name
      let roadNameElem = document.createElement('span');
      roadNameElem.classList.add('road_name');
      roadNameElem.innerHTML = props['road_name'];

      step.appendChild(roadNameElem);

      stepList.appendChild(step);
      startMovement = props['end_movement'];
    }
    let endDateTime = moment(startDateTime).add(aggregatedTimeMin, 'minutes');
    let duration = moment.duration(endDateTime.diff(startDateTime));
    // TODO nicer texts
    metadataContainer.innerHTML = `
      <span>Departure: ${startDateTime}</span>
      <span>Arrival: ${endDateTime}</span>
      <span>Total Time: ${duration.format('hh:mm', { trim: false })}</span`;

    itineraryContainer.classList.add('visible');

  };

})();

document.getElementById('itinerary_action').addEventListener('submit', (e) => {
  e.preventDefault();

  fetch(`${settings.geojsonServerUrl}/request`, {
    credentials: 'include',
    mode: 'cors',
    method: 'post',
    body: new FormData(e.target),
  }).then(response => {
    if (!response.ok) {
      return response.text().then(text => {
        throw new Error('Error when fetching /request: ' + text);
      });
    }
    return response.json();
  }).then(json => {
    let geoJson = json.roadmaps[0]; // at the moment, we display only the first itinerary
    itineraryFeatures.clear();
    itineraryFeatures.extend((new ol.format.GeoJSON({featureProjection: 'EPSG:3857'})).readFeatures(geoJson));
    updateItinerary(geoJson);
  }).catch(e => logError(e.message));

  return false;
});

// Init form
fetch(`${settings.geojsonServerUrl}/list_transportmodes`).then(r => {
  if (!r.ok) {
    throw new Error('Error when fetching /list_transportmodes');
  }
  return r.json();
}).then(json => {
  let modeContainer = document.getElementById('allowed_modes');
  for (let elem of json) {
    let div = document.createElement('div');
    div.classList.add('label_and_checkbox');
    div.innerHTML = `
      <label for="allowed_modes_${elem.id}" style="border-bottom: ${TRANSPORT_MODE_COLOR[elem.id]} 0.2rem solid">${elem.name}</label>
      <input type="checkbox" id="allowed_modes_${elem.id}" name="allowed_modes" value="${elem.id}">
    `;
    modeContainer.appendChild(div);
  }
}).catch(e => logError(e.message));

function logError(message) {
  let debugDiv = document.querySelector('#debug');
  debugDiv.appendChild(document.createTextNode(message));
  debugDiv.appendChild(document.createElement('br'));
  debugDiv.scrollTop = debugDiv.scrollHeight;
}

console.log('loaded');
