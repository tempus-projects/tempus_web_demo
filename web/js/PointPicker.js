/**
 * This module gives points picker on open layer
 *
 * Accessible fields:
 * - this.marker : an ol.Feature representing the point
 */
const ol = require('openlayers');
let EventDispatcher = require('eventdispatcher.js');

/**
 * Create an PointPicker on a source
 *
 * @param {ol.map} map - an open layer map object
 * @param {ol.source.Vector} source - an open layer vector source where the point will be added
 * @param {string} text - the label of the point on the map
 * @
 */
function PointPicker(map, source, style) {

  this._map = map;
  // not placed by default
  let added = false;

  // ol boilerplate
  this.marker = new ol.Feature({});
  this.marker.setStyle(style);

  source.addFeature(this.marker);
}

PointPicker.prototype = Object.create(EventDispatcher.prototype);
PointPicker.prototype.constructor = PointPicker;

PointPicker.prototype.startPicking = function() {
  let map = this._map;
  this._map.getTargetElement().style.cursor = 'crosshair';
  this._map.un('click', this._map._currentPlaceFn);
  this._map._currentPlaceFn = evt => {
    this._map.getTargetElement().style.cursor = '';

    this.place(evt.coordinate);
    this.dispatchEvent( {type: 'placed', coordinates: evt.coordinate});
    console.log('dispatchedEvent' ,evt.coordinate);

    delete this._map._currentPlaceFn;
  };
  this._map.once('click', map._currentPlaceFn);
};

PointPicker.prototype.place = function(coordinate, crs) {
  if (!coordinate.length || coordinate.length < 2) {
    console.warning(`${coordinate} is not a coordinate object`);
    return;
  }
  let projCoordinate = coordinate;
  if (crs) {
    projCoordinate = ol.proj.transform(coordinate, crs, 'EPSG:3857');
  }
  if (!this.marker.getGeometry()) {
    this.marker.setGeometry(new ol.geom.Point(projCoordinate));
  } else {
    this.marker.getGeometry().setCoordinates(projCoordinate);
  }
};

PointPicker.prototype.getCoordinates = function (crs) {
  if (!this.marker || !this.marker.getGeometry()) {
    return false;
  }
  if (crs && crs !== 'EPSG:3857') {
    return ol.proj.transform(this.marker.getGeometry().getCoordinates(), 'EPSG:3857', crs);
  } else {
    return this.marker.getGeometry().getCoordinates();
  }
};

module.exports = PointPicker;
